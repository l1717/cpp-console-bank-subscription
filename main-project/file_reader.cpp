#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

void read(const char* file_name, bank* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            bank* item = new bank;
            file >> item->Bank_Name;
            file >> item->Buy;
            file >> item->Sell;
            file >> item->Adress;
            file >> tmp_buffer;
            file.read(tmp_buffer, 1);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}