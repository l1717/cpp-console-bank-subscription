#ifndef BANK_SUBSCRIPTION_H
#define BANK_SUBSCRIPTION_H

#include "constants.h"
struct bank {
	char Bank_Name[MAX_STRING_SIZE];
	double Buy;
	double Sell;
	char Adress[MAX_STRING_SIZE];
};
#endif